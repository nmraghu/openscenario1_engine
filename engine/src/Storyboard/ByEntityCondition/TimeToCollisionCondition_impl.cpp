/********************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Storyboard/ByEntityCondition/TimeToCollisionCondition_impl.h"

#include <MantleAPI/Common/position.h>
#include <MantleAPI/Traffic/i_entity.h>
#include <MantleAPI/Traffic/i_entity_repository.h>
#include <units.h>

#include <stdexcept>
#include <variant>

#include "Utils/EntityUtils.h"

namespace OpenScenarioEngine::v1_1
{
using namespace OPENSCENARIO;

bool TimeToCollisionCondition::IsSatisfied() const
{
  const auto& triggeringEntity = EntityUtils::GetEntityByName(mantle.environment, values.triggeringEntity);

  if (auto entity = std::get_if<Entity>(&values.timeToCollisionConditionTarget); entity)
  {
    const auto& referenceEntity = EntityUtils::GetEntityByName(mantle.environment, *entity);
    const auto ttc = CalculateTTC(triggeringEntity, referenceEntity);
    return values.rule.IsSatisfied(ttc.value());
  }
  if (auto pose = std::get_if<std::optional<mantle_api::Pose>>(&values.timeToCollisionConditionTarget); pose)
  {
    if (pose)
    {
      const auto ttc = CalculateTTC(triggeringEntity, (*pose).value());
      return values.rule.IsSatisfied(ttc.value());
    }
    std::cout << "TimeToCollisionCondition: Unable to resolve position.\n";
    return false;
  }

  throw std::runtime_error("TimeToCollisionCondition: timeToCollisionConditionTarget could not be resolved");
}

units::time::second_t TimeToCollisionCondition::CalculateTTC(const mantle_api::IEntity& triggeringEntity, mantle_api::Pose targetPose) const
{
  std::cout << "TimeToCollision with respect to pose not implemented yet (returning 0).\n";
  return units::make_unit<units::time::second_t>(0);
}

units::time::second_t TimeToCollisionCondition::CalculateTTC(const mantle_api::IEntity& entity, const mantle_api::IEntity& targetEntity) const
{
  std::cout << "TimeToCollision with respect to target entity not implemented yet (returning 0).\n";
  return units::make_unit<units::time::second_t>(0);
}

}  // namespace OpenScenarioEngine::v1_1
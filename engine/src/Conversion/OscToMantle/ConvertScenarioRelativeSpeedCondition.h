#pragma once

#include <openScenarioLib/generated/v1_1/api/ApiClassInterfacesV1_1.h>

#include <memory>
#include <string>

#include "Conversion/OscToMantle/ConvertScenarioEntity.h"

namespace OPENSCENARIO
{
struct RelativeSpeedCondition
{
  Entity entity;
  Rule rule;
  double value;
};

inline RelativeSpeedCondition ConvertScenarioRelativeSpeedCondition(
    std::shared_ptr<mantle_api::IEnvironment> environment,
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IRelativeSpeedCondition> relativeSpeedCondition)
{
  return {
      ConvertScenarioEntity(relativeSpeedCondition->GetEntityRef()),
      relativeSpeedCondition->GetRule(),
      relativeSpeedCondition->GetValue()

  };
}

}  // namespace OPENSCENARIO
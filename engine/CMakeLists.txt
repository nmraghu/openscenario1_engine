# *****************************************************************************
# Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the terms
# of the Eclipse Public License 2.0 which is available at
# https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
# ******************************************************************************

cmake_minimum_required(VERSION 3.16.3)

# Add the custom CMake modules to CMake's module path
set(CMAKE_MODULE_PATH ${CMAKE_CURRENT_LIST_DIR}/cmake)

set(PROJECT_NAME OpenScenarioEngine)
project(${PROJECT_NAME} VERSION 0.3)

# YASE
set(DEP_YASE ${CMAKE_PREFIX_PATH}/middle_end/agnostic_behavior_tree)

set(DEP_YASE_SRC
    # Basic definitions
    ${DEP_YASE}/src/action_node.cpp
    ${DEP_YASE}/src/behavior_node.cpp
    ${DEP_YASE}/src/composite_node.cpp
    ${DEP_YASE}/src/composite/parallel_node.cpp
    ${DEP_YASE}/src/composite/sequence_node.cpp
    ${DEP_YASE}/src/decorator_node.cpp
    ${DEP_YASE}/src/decorator/stop_at_node.cpp
    ${DEP_YASE}/src/scoped_blackboard.cpp
    # Utils
    ${DEP_YASE}/src/utils/condition.cpp
    ${DEP_YASE}/src/utils/tree_analyzer.cpp
    ${DEP_YASE}/src/utils/tree_print.cpp
    ${DEP_YASE}/src/utils/visitors.cpp
)

# RA Consulting OpenSCENARIO API https://github.com/RA-Consulting-GmbH/openscenario.api.test/
set(DEP_OSC_API ${CMAKE_PREFIX_PATH}/openscenario_api/cpp)

# MantleAPI https://gitlab.eclipse.org/eclipse/simopenpass/scenario_api
set(DEP_MANTLE ${CMAKE_PREFIX_PATH}/scenario_api)

# units https://github.com/nholthaus/units (dependency of MantleAPI)
set(DEP_UNITS ${CMAKE_PREFIX_PATH}/units)

# googlemock
include(CPM)
CPMAddPackage(
  NAME googletest
  GITHUB_REPOSITORY google/googletest
  GIT_TAG release-1.12.1
  VERSION 1.12.1
  OPTIONS "INSTALL_GTEST OFF" "gtest_force_shared_crt ON"
)

# see https://stackoverflow.com/a/58495612
set(CMAKE_INSTALL_RPATH $ORIGIN)

add_library(${PROJECT_NAME} SHARED)

target_compile_features(${PROJECT_NAME} PRIVATE cxx_std_17)

find_package(OpenScenarioAPI REQUIRED)
find_package(Antlr4Runtime REQUIRED)

include(cmake/generated_files.cmake)

target_sources(
  ${PROJECT_NAME} PRIVATE ${${PROJECT_NAME}_SOURCES} ${${PROJECT_NAME}_HEADERS} ${DEP_YASE_SRC}
                          ${CMAKE_CURRENT_LIST_DIR}/include/OpenScenarioEngine/OpenScenarioEngine.h
)

target_include_directories(
  ${PROJECT_NAME}
  PUBLIC $<INSTALL_INTERFACE:include>
  PRIVATE gen src ${DEP_UNITS}/include ${DEP_MANTLE}/include ${DEP_YASE}/include
          $<BUILD_INTERFACE:${CMAKE_CURRENT_LIST_DIR}/include>
)

target_link_libraries(${PROJECT_NAME} PUBLIC openscenario_api::shared antlr4_runtime::shared)

find_program(CCACHE_FOUND ccache)

if(CCACHE_FOUND)
  message("Found ccache")
  set_property(GLOBAL PROPERTY RULE_LAUNCH_COMPILE ccache)
endif(CCACHE_FOUND)

# Add -O0 to remove optimizations when using gcc
if(CMAKE_COMPILER_IS_GNUCC)
  message("Disable optimization")
  set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -O0")
  set(CMAKE_C_FLAGS_DEBUG "${CMAKE_C_FLAGS_DEBUG} -O0")
endif(CMAKE_COMPILER_IS_GNUCC)

include(GenerateExportHeader)
generate_export_header(${PROJECT_NAME})

install(TARGETS ${PROJECT_NAME} EXPORT ${PROJECT_NAME}Targets)

install(FILES ${CMAKE_CURRENT_LIST_DIR}/include/OpenScenarioEngine/OpenScenarioEngine.h
        DESTINATION include/OpenScenarioEngine
)

include(CMakePackageConfigHelpers)
write_basic_package_version_file(
  "${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}/cmake/${PROJECT_NAME}ConfigVersion.cmake"
  VERSION ${Upstream_VERSION}
  COMPATIBILITY AnyNewerVersion
)

export(
  EXPORT ${PROJECT_NAME}Targets
  FILE "${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}/cmake/${PROJECT_NAME}Targets.cmake"
  NAMESPACE OpenScenarioEngine::
)

configure_file(
  cmake/${PROJECT_NAME}Config.cmake "${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}/cmake/${PROJECT_NAME}Config.cmake"
  COPYONLY
)

set(ConfigPackageLocation lib/cmake/${PROJECT_NAME})

install(FILES cmake/FindAntlr4Runtime.cmake cmake/FindOpenScenarioAPI.cmake DESTINATION ${ConfigPackageLocation}/deps)

install(FILES cmake/${PROJECT_NAME}Config.cmake DESTINATION ${ConfigPackageLocation})

install(
  EXPORT ${PROJECT_NAME}Targets
  NAMESPACE OpenScenarioEngine::
  DESTINATION ${ConfigPackageLocation}
)

# DEMO
add_executable(${PROJECT_NAME}Demo EXCLUDE_FROM_ALL ${CMAKE_CURRENT_LIST_DIR}/demo/main.cpp)

target_include_directories(
  ${PROJECT_NAME}Demo
  PRIVATE src
          $<BUILD_INTERFACE:${CMAKE_CURRENT_LIST_DIR}/include>
          ${DEP_MANTLE}/test
          ${DEP_UNITS}/include
          ${DEP_MANTLE}/include
          ${DEP_YASE}/include
          ${DEP_OSC_API}
          # in order of missing includes ;)
          ${DEP_OSC_API}/openScenarioLib/src/common # INamedReference
          ${DEP_OSC_API}/openScenarioLib/src/api # IOpenScenarioModelElement.h
          ${DEP_OSC_API}/common # MemLeakDetection
          ${DEP_OSC_API}/openScenarioLib/src/checker/tree # ITreeContext.h
)

add_dependencies(${PROJECT_NAME}Demo ${PROJECT_NAME})
target_compile_features(${PROJECT_NAME}Demo PRIVATE cxx_std_17)

target_link_libraries(${PROJECT_NAME}Demo ${PROJECT_NAME} GTest::gmock_main)

# ---------------

enable_testing()

add_executable(${PROJECT_NAME}Test "")

target_sources(
  ${PROJECT_NAME}Test
  PRIVATE ${SRC_OSE_IMPLEMENTATION}
          ${SRC_YASE_MIDDLE_END}
          ${DEP_YASE}/src/decorator/data_declaration_node.cpp
          ${CMAKE_CURRENT_LIST_DIR}/tests/OpenScenarioEngineTest.cpp
          ${CMAKE_CURRENT_LIST_DIR}/tests/Node/ConditionNodeTest.cpp
          ${CMAKE_CURRENT_LIST_DIR}/tests/Node/ConditionsNodeTest.cpp
          ${CMAKE_CURRENT_LIST_DIR}/tests/Node/ConditionGroupsNodeTest.cpp
          ${CMAKE_CURRENT_LIST_DIR}/tests/Storyboard/GenericAction/AssignControllerActionTest.cpp
          ${CMAKE_CURRENT_LIST_DIR}/tests/Storyboard/GenericAction/DeleteEntityActionTest.cpp
          ${CMAKE_CURRENT_LIST_DIR}/tests/Storyboard/GenericAction/TeleportActionTest.cpp
          ${CMAKE_CURRENT_LIST_DIR}/tests/Storyboard/GenericAction/TrafficSignalStateActionTest.cpp
          ${CMAKE_CURRENT_LIST_DIR}/tests/Storyboard/GenericAction/AssignRouteActionTest.cpp
          ${CMAKE_CURRENT_LIST_DIR}/tests/Storyboard/ByEntityCondition/ReachPositionConditionTest.cpp
          ${CMAKE_CURRENT_LIST_DIR}/tests/Storyboard/ByEntityCondition/RelativeDistanceConditionTest.cpp
          ${CMAKE_CURRENT_LIST_DIR}/tests/Storyboard/ByEntityCondition/RelativeSpeedConditionTest.cpp
          ${CMAKE_CURRENT_LIST_DIR}/tests/Storyboard/ByValueCondition/SimulationTimeConditionTest.cpp
          ${CMAKE_CURRENT_LIST_DIR}/tests/Storyboard/MotionControlAction/FollowTrajectoryActionTest.cpp
          ${CMAKE_CURRENT_LIST_DIR}/tests/Storyboard/MotionControlAction/LaneChangeActionTest.cpp
          ${CMAKE_CURRENT_LIST_DIR}/tests/Storyboard/MotionControlAction/SpeedActionTest.cpp
          ${CMAKE_CURRENT_LIST_DIR}/tests/TrafficSignalParsing/TrafficPhaseNodeTest.cpp
          ${CMAKE_CURRENT_LIST_DIR}/tests/TrafficSignalParsing/TreeGenerationTest.cpp
          ${CMAKE_CURRENT_LIST_DIR}/tests/Conversion/OscToMantle/ConvertScenarioAbsoluteTargetLaneTest.cpp
          ${CMAKE_CURRENT_LIST_DIR}/tests/Conversion/OscToMantle/ConvertScenarioCoordinateSystemTest.cpp
          ${CMAKE_CURRENT_LIST_DIR}/tests/Conversion/OscToMantle/ConvertScenarioPositionTest.cpp
          ${CMAKE_CURRENT_LIST_DIR}/tests/Conversion/OscToMantle/ConvertScenarioRelativeDistanceTypeTest.cpp
          ${CMAKE_CURRENT_LIST_DIR}/tests/Conversion/OscToMantle/ConvertScenarioRelativeTargetLaneTest.cpp
          ${CMAKE_CURRENT_LIST_DIR}/tests/Conversion/OscToMantle/ConvertScenarioRuleTest.cpp
          ${CMAKE_CURRENT_LIST_DIR}/tests/Conversion/OscToMantle/ConvertScenarioSpeedActionTargetTest.cpp
          ${CMAKE_CURRENT_LIST_DIR}/tests/Conversion/OscToMantle/ConvertScenarioTimeReferenceTest.cpp
          ${CMAKE_CURRENT_LIST_DIR}/tests/Conversion/OscToMantle/ConvertScenarioTimeToCollisionConditionTargetTest.cpp
          ${CMAKE_CURRENT_LIST_DIR}/tests/Conversion/OscToMantle/ConvertScenarioTrajectoryRefTest.cpp
)

add_custom_command(TARGET ${PROJECT_NAME}Test
                    POST_BUILD
                    COMMAND ${CMAKE_COMMAND} -E copy_directory
                      ${CMAKE_CURRENT_SOURCE_DIR}/tests/data
                      ${CMAKE_CURRENT_BINARY_DIR}/data
)

target_compile_features(${PROJECT_NAME}Test PRIVATE cxx_std_17)

target_include_directories(
  ${PROJECT_NAME}Test
  PRIVATE gen
          src
          tests
          $<BUILD_INTERFACE:${CMAKE_CURRENT_LIST_DIR}/include>
          ${DEP_UNITS}/include
          ${DEP_MANTLE}/include
          ${DEP_MANTLE}/test
          ${DEP_YASE}/include
)

target_link_libraries(
  ${PROJECT_NAME}Test
  PRIVATE ${PROJECT_NAME}
          openscenario_api::shared
          antlr4_runtime::shared
          GTest::gmock_main
          pthread
)

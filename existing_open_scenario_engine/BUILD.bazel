load("@rules_cc//cc:defs.bzl", "cc_library", "cc_test")

################################################################################
# Copyright (c) 2021-2022, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made
# available under the terms of the Eclipse Public License 2.0
# which is available at https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

cc_library(
    name = "open_scenario_engine",
    srcs = glob([
        "src/**/*.h",
        "src/**/*.cpp",
    ]),
    hdrs = glob(["include/**/*.h"]),
    includes = [
        "include",
        "src",
    ],
    visibility = ["//visibility:public"],
    deps = [
        "@mantle_api",
        "@open_scenario_parser_shared",
    ],
)

filegroup(
    name = "open_scenario_engine_test_data",
    data = glob(["test/data/**/*.*"]),
    visibility = ["//visibility:public"],
)

cc_library(
    name = "open_scenario_builders",
    srcs = glob(["test/builders/*.cpp"]),
    hdrs = glob(["test/builders/*.h"]),
    includes = ["test"],
    deps = ["@open_scenario_parser_shared"],
)

cc_library(
    name = "open_scenario_engine_test_utils",
    hdrs = ["test/TestUtils.h"],
    includes = ["test"],
    linkopts = ["-lstdc++fs"],
    visibility = ["//visibility:public"],
    deps = [
        ":open_scenario_engine",
        "@googletest//:gtest_main",
        "@mantle_api//:test_utils",
    ],
)

cc_test(
    name = "open_scenario_engine_test",
    timeout = "short",
    srcs = ["test/OpenScenarioEngineTest.cpp"],
    data = [":open_scenario_engine_test_data"],
    tags = ["test"],
    deps = [
        ":open_scenario_builders",
        ":open_scenario_engine",
        ":open_scenario_engine_test_utils",
        "@googletest//:gtest_main",
    ],
)

cc_test(
    name = "entity_creator_test",
    timeout = "short",
    srcs = ["test/EntityCreatorTest.cpp"],
    data = [":open_scenario_engine_test_data"],
    tags = ["test"],
    deps = [
        ":open_scenario_engine",
        ":open_scenario_engine_test_utils",
        "@googletest//:gtest_main",
    ],
)

cc_test(
    name = "controller_creator_test",
    timeout = "short",
    srcs = ["test/ControllerCreatorTest.cpp"],
    data = [":open_scenario_engine_test_data"],
    tags = ["test"],
    deps = [
        ":open_scenario_engine_test_utils",
        "@googletest//:gtest_main",
    ],
)

cc_test(
    name = "storyboard_test",
    timeout = "short",
    srcs = ["test/StoryboardTest.cpp"],
    data = [":open_scenario_engine_test_data"],
    tags = ["test"],
    deps = [
        ":open_scenario_builders",
        ":open_scenario_engine",
        ":open_scenario_engine_test_utils",
        "@googletest//:gtest_main",
    ],
)

cc_test(
    name = "state_machine_test",
    timeout = "short",
    srcs = ["test/StateMachineTest.cpp"],
    data = [":open_scenario_engine_test_data"],
    tags = ["test"],
    deps = [
        ":open_scenario_engine",
        "@googletest//:gtest_main",
        "@mantle_api//:test_utils",
    ],
)

cc_test(
    name = "state_test",
    timeout = "short",
    srcs = ["test/StateTest.cpp"],
    data = [":open_scenario_engine_test_data"],
    tags = ["test"],
    deps = [
        ":open_scenario_builders",
        ":open_scenario_engine",
        "@googletest//:gtest_main",
        "@mantle_api//:test_utils",
    ],
)

cc_test(
    name = "teleport_action_test",
    timeout = "short",
    srcs = ["test/TeleportActionTest.cpp"],
    tags = ["test"],
    deps = [
        ":open_scenario_builders",
        ":open_scenario_engine",
        ":open_scenario_engine_test_utils",
        "@googletest//:gtest_main",
        "@mantle_api//:test_utils",
    ],
)

cc_test(
    name = "visibility_action_test",
    timeout = "short",
    srcs = ["test/VisibilityActionTest.cpp"],
    tags = ["test"],
    deps = [
        ":open_scenario_builders",
        ":open_scenario_engine",
        ":open_scenario_engine_test_utils",
        "@googletest//:gtest_main",
        "@mantle_api//:test_utils",
    ],
)

cc_test(
    name = "motion_control_action_test",
    timeout = "short",
    srcs = ["test/MotionControlActionTest.cpp"],
    tags = ["test"],
    deps = [":open_scenario_engine_test_utils"],
)

cc_test(
    name = "speed_action_test",
    timeout = "short",
    srcs = ["test/SpeedActionTest.cpp"],
    tags = ["test"],
    deps = [
        ":open_scenario_builders",
        ":open_scenario_engine",
        ":open_scenario_engine_test_utils",
        "@googletest//:gtest_main",
        "@mantle_api//:test_utils",
    ],
)

cc_test(
    name = "constants_test",
    timeout = "short",
    srcs = ["test/ConstantsTest.cpp"],
    tags = ["test"],
    deps = [
        ":open_scenario_engine",
        "@googletest//:gtest_main",
    ],
)

cc_test(
    name = "rule_test",
    timeout = "short",
    srcs = ["test/RuleTest.cpp"],
    tags = ["test"],
    deps = [
        ":open_scenario_engine",
        "@googletest//:gtest_main",
    ],
)

cc_test(
    name = "trigger_test",
    timeout = "short",
    srcs = ["test/TriggerTest.cpp"],
    tags = ["test"],
    deps = [
        ":open_scenario_builders",
        ":open_scenario_engine_test_utils",
    ],
)

cc_test(
    name = "event_test",
    timeout = "short",
    srcs = ["test/EventTest.cpp"],
    tags = ["test"],
    deps = [
        ":open_scenario_builders",
        ":open_scenario_engine_test_utils",
    ],
)

cc_test(
    name = "convert_scenario_position_test",
    timeout = "short",
    srcs = ["test/ConvertScenarioPositionTest.cpp"],
    tags = ["test"],
    deps = [
        ":open_scenario_builders",
        ":open_scenario_engine_test_utils",
    ],
)

cc_test(
    name = "traffic_signal_state_action_test",
    timeout = "short",
    srcs = ["test/TrafficSignalStateActionTest.cpp"],
    tags = ["test"],
    deps = [
        ":open_scenario_builders",
        ":open_scenario_engine_test_utils",
    ],
)

cc_test(
    name = "lane_change_action_test",
    timeout = "short",
    srcs = ["test/LaneChangeActionTest.cpp"],
    tags = ["test"],
    deps = [
        ":open_scenario_builders",
        ":open_scenario_engine",
        ":open_scenario_engine_test_utils",
        "@googletest//:gtest_main",
        "@mantle_api//:test_utils",
    ],
)

cc_test(
    name = "longitudinal_distance_action_test",
    timeout = "short",
    srcs = ["test/LongitudinalDistanceActionTest.cpp"],
    tags = ["test"],
    deps = [
        ":open_scenario_builders",
        ":open_scenario_engine",
        ":open_scenario_engine_test_utils",
        "@googletest//:gtest_main",
        "@mantle_api//:test_utils",
    ],
)

cc_test(
    name = "assign_route_action_test",
    timeout = "short",
    srcs = ["test/AssignRouteActionTest.cpp"],
    tags = ["test"],
    deps = [
        ":open_scenario_builders",
        ":open_scenario_engine",
        ":open_scenario_engine_test_utils",
        "@googletest//:gtest_main",
        "@mantle_api//:test_utils",
    ],
)

cc_test(
    name = "custom_command_action_test",
    timeout = "short",
    srcs = ["test/CustomCommandActionTest.cpp"],
    tags = ["test"],
    deps = [
        ":open_scenario_builders",
        ":open_scenario_engine",
        ":open_scenario_engine_test_utils",
        "@googletest//:gtest_main",
        "@mantle_api//:test_utils",
    ],
)

cc_test(
    name = "follow_trajectory_action_test",
    timeout = "short",
    srcs = ["test/FollowTrajectoryActionTest.cpp"],
    tags = ["test"],
    deps = [
        ":open_scenario_builders",
        ":open_scenario_engine",
        ":open_scenario_engine_test_utils",
        "@googletest//:gtest_main",
        "@mantle_api//:test_utils",
    ],
)

cc_test(
    name = "relative_distance_condition_test",
    timeout = "short",
    srcs = ["test/RelativeDistanceConditionTest.cpp"],
    tags = ["test"],
    deps = [
        ":open_scenario_builders",
        ":open_scenario_engine",
        ":open_scenario_engine_test_utils",
        "@googletest//:gtest_main",
        "@mantle_api//:test_utils",
    ],
)

cc_test(
    name = "entity_utils_test",
    timeout = "short",
    srcs = ["test/EntityUtilsTest.cpp"],
    tags = ["test"],
    deps = [
        ":open_scenario_engine",
        ":open_scenario_engine_test_utils",
        "@googletest//:gtest_main",
        "@mantle_api//:test_utils",
    ],
)

cc_test(
    name = "user_defined_value_condition_test",
    timeout = "short",
    srcs = ["test/UserDefinedValueConditionTest.cpp"],
    tags = ["test"],
    deps = [
        ":open_scenario_engine",
        ":open_scenario_engine_test_utils",
        "@googletest//:gtest_main",
    ],
)

cc_test(
    name = "by_entity_conditions_base_test",
    timeout = "short",
    srcs = ["test/ByEntityConditionsBaseTest.cpp"],
    tags = ["test"],
    deps = [
        ":open_scenario_builders",
        ":open_scenario_engine",
        ":open_scenario_engine_test_utils",
        "@googletest//:gtest_main",
        "@mantle_api//:test_utils",
    ],
)

cc_test(
    name = "time_headway_condition_test",
    timeout = "short",
    srcs = ["test/TimeHeadwayConditionTest.cpp"],
    tags = ["test"],
    deps = [
        ":open_scenario_builders",
        ":open_scenario_engine",
        ":open_scenario_engine_test_utils",
        "@googletest//:gtest_main",
        "@mantle_api//:test_utils",
    ],
)

cc_test(
    name = "condition_test",
    timeout = "short",
    srcs = ["test/ConditionTest.cpp"],
    tags = ["test"],
    deps = [
        ":open_scenario_engine",
        ":open_scenario_engine_test_utils",
        "@googletest//:gtest_main",
        "@mantle_api//:test_utils",
    ],
)

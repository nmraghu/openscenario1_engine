
cmake_minimum_required(VERSION 3.21)

project(OpenScenarioEngine C CXX)

FIND_PATH(OPENSCENARIOPARSER_INCLUDE_DIR "openScenarioLib/generated/v1_1/api/ApiClassInterfacesV1_1.h"
  PATHS
    ${PREFIX_PATH}
  DOC "OPENSCENARIOPARSER - Headers"
)

FIND_PATH(MANTLE_INCLUDE_DIR "MantleAPI/Execution/i_environment.h"
  PATHS
    ${PREFIX_PATH}
  DOC "MANTLE - Headers"
)

FIND_PATH(MANTLE_TEST_INCLUDE_DIR "MantleAPI/Test/test_utils.h"
  PATHS
    ${PREFIX_PATH}
  DOC "MANTLE - Headers"
)

FIND_PATH(UNITS_INCLUDE_DIR "units.h"
  PATHS
    ${PREFIX_PATH}
  DOC "UNITS - Headers"
)

unset( XOSC_LIB CACHE )
unset( ANTLR4_LIB CACHE )
unset( EXP_LIB CACHE )
find_library( XOSC_LIB name "${LIB_PREFIX}OpenScenarioLib${LIB_SUFFIX}" HINTS "${OPENSCENARIOPARSER_INCLUDE_DIR}/../lib/Linux" )
find_library( ANTLR4_LIB name "${LIB_PREFIX}antlr4-runtime${LIB_SUFFIX}" HINTS "${OPENSCENARIOPARSER_INCLUDE_DIR}/../lib/Linux" )
find_library( EXP_LIB name "${LIB_PREFIX}ExpressionsLib${LIB_SUFFIX}" HINTS "${OPENSCENARIOPARSER_INCLUDE_DIR}/../lib/Linux" )

if(NOT XOSC_LIB)
  message(FATAL_ERROR "XOSC_LIB library not found")
endif()

if(NOT ANTLR4_LIB)
  message(FATAL_ERROR "ANTLR4_LIB library not found")
endif()

if(NOT EXP_LIB)
  message(FATAL_ERROR "EXP_LIB library not found")
endif()

  enable_testing()
  add_subdirectory(test) 

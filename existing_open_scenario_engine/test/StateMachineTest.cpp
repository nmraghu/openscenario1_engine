/*******************************************************************************
 * Copyright (c) 2021, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "StateMachine/StateMachine.h"

#include <gmock/gmock.h>
#include <gtest/gtest.h>

namespace OPENSCENARIO
{

class MockState : public IState
{
  public:
    MOCK_METHOD(void, Step, (), (override));

    MOCK_METHOD(void, SetContext, (IStateMachine & context), (override));

    MOCK_METHOD(void, SetTransitionToState, (IState & next_state), (override));
};

TEST(StateMachineTest, GivenStateMachineWithNoActiveState_WhenStep_ThenNoThrow)
{
    StateMachine state_machine{};

    EXPECT_NO_THROW(state_machine.Step());
}

TEST(StateMachineTest, GivenStateMachineWithActiveState_WhenStep_ThenNoThrow)
{
    StateMachine state_machine{};

    MockState mock_state{};
    state_machine.SetActiveState(mock_state);

    EXPECT_NO_THROW(state_machine.Step());
}

TEST(StateMachineTest, GivenStateMachineWithActiveState_WhenStep_ThenActiveStateIsStepped)
{
    StateMachine state_machine{};

    MockState mock_state{};
    EXPECT_CALL(mock_state, Step()).Times(1);
    state_machine.SetActiveState(mock_state);

    state_machine.Step();
}

TEST(StateMachineTest, GivenStateMachineWithActiveState_WhenGettingActiveState_ThenNotNull)
{
    StateMachine state_machine{};

    MockState mock_state{};
    state_machine.SetActiveState(mock_state);

    EXPECT_FALSE(state_machine.GetActiveState() == nullptr);
}

TEST(StateMachineTest, GivenStateMachineWithoutActiveState_WhenGettingActiveState_ThenNull)
{
    StateMachine state_machine{};

    EXPECT_TRUE(state_machine.GetActiveState() == nullptr);
}

}  // namespace OPENSCENARIO

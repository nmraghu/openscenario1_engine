/*******************************************************************************
 * Copyright (c) 2021, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "StateMachine/StateMachine.h"
#include "StoryboardElementStates/StandbyState.h"

#include <gmock/gmock.h>
#include <gtest/gtest.h>

namespace OPENSCENARIO
{

class MockStateMachine : public IStateMachine
{
  public:
    MOCK_METHOD(void, Step, (), (override));
    void SetActiveState(IState& new_active_state) override { active_state_ = &new_active_state; }
    const IState* GetActiveState() override { return active_state_; }

  private:
    IState* active_state_;
};

class MockState : public IState
{
  public:
    MOCK_METHOD(void, Step, (), (override));

    MOCK_METHOD(void, SetContext, (IStateMachine & context), (override));

    MOCK_METHOD(void, SetTransitionToState, (IState & next_state), (override));
};

/// ==============================
/// InitState

TEST(StandbyStateTest, GivenStandbyStateWithoutContext_WhenStepped_ThenThrows)
{
    StandbyState standby_state{};

    EXPECT_THROW(standby_state.Step(), std::runtime_error);
}

TEST(StandbyStateTest, GivenStandbyStateWithoutTransitionState_WhenStepped_ThenThrows)
{
    MockStateMachine context{};
    StandbyState standby_state{};
    standby_state.SetContext(context);

    EXPECT_THROW(standby_state.Step(), std::runtime_error);
}

TEST(StandbyStateTest, GivenStandbyStateWithTransition_WhenStepped_ThenContextHasTransitionState)
{
    MockStateMachine context{};
    MockState transition_state{};

    StandbyState standby_state{};
    standby_state.SetContext(context);
    standby_state.SetTransitionToState(transition_state);

    standby_state.Step();

    EXPECT_EQ(context.GetActiveState(), &transition_state);
}

// TODO: Add tests for each of the states

}  // namespace OPENSCENARIO
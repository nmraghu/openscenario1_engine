/*******************************************************************************
 * Copyright (c) 2021, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Storyboard/Event.h"
#include "TestUtils.h"
#include "builders/ActionBuilder.h"
#include "builders/PositionBuilder.h"
#include "builders/storyboard_builder.h"

#include <gtest/gtest.h>

namespace OPENSCENARIO
{
using namespace OPENSCENARIO::TESTING;

class EventSUT : public Event
{
  public:
    EventSUT(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IEvent> event_data,
             std::shared_ptr<mantle_api::IEnvironment> environment,
             std::vector<std::string> actors)
        : Event(event_data, environment, actors)
    {
    }

    MOCK_METHOD(void, ExecuteStartTransition, (), (override));

    std::vector<std::shared_ptr<Action>> GetActions() { return actions; }
};

class EventTestFixture : public OpenScenarioEngineLibraryTestBase
{
  protected:
    void SetUp() override { OpenScenarioEngineLibraryTestBase::SetUp(); }
};

std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IActionWriter> GetFakeAction()
{
    return FakeActionBuilder()
        .WithPrivateAction(
            FakePrivateActionBuilder()
                .WithTeleportAction(
                    FakeTeleportActionBuilder()
                        .WithPosition(FakePositionBuilder()
                                          .WithLanePosition(FakeLanePositionBuilder("1", "2", 3.0, 4.0).Build())
                                          .Build())
                        .Build())
                .Build())
        .Build();
}

std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IEventWriter> GetFakeEvent(const FakeByValueCondition& by_value_condition)
{
    return FakeEventBuilder()
        .WithStartTrigger(
            FakeTriggerBuilder()
                .WithConditionGroup(
                    FakeConditionGroupBuilder()
                        .WithCondition(FakeConditionBuilder{}.WithByValueCondition(by_value_condition).Build())
                        .Build())
                .Build())
        .WithAction(GetFakeAction())
        .Build();
}

TEST_F(EventTestFixture, GivenEventWithSatisfiedStartTrigger_WhenStep_ThenEventAndActionAreStarted)
{
    auto by_value_condition = FakeByValueConditionBuilder{}
                                  .WithSimulationTimeCondition(FakeSimulationTimeCondition{
                                      NET_ASAM_OPENSCENARIO::v1_1::Rule::RuleEnum::EQUAL_TO, 0.0})
                                  .Build();
    auto fake_event = GetFakeEvent(by_value_condition);

    OPENSCENARIO::EventSUT event(fake_event, env_, std::vector<std::string>{"Ego"});

    EXPECT_CALL(event, ExecuteStartTransition).Times(1);
    auto actions = event.GetActions();
    EXPECT_EQ(1, actions.size());
    EXPECT_FALSE(event.isRunning());
    EXPECT_TRUE(actions[0]->isStandby());
    event.Step();
    EXPECT_TRUE(event.isRunning());
    EXPECT_FALSE(actions[0]->isStandby());
}

TEST_F(EventTestFixture, GivenEventWithSatisfiedStartTrigger_WhenStepMultipleTimes_ThenOnlyStartedOnce)
{
    auto by_value_condition = FakeByValueConditionBuilder{}
                                  .WithSimulationTimeCondition(FakeSimulationTimeCondition{
                                      NET_ASAM_OPENSCENARIO::v1_1::Rule::RuleEnum::EQUAL_TO, 0.0})
                                  .Build();
    auto fake_event = GetFakeEvent(by_value_condition);

    OPENSCENARIO::EventSUT event(fake_event, env_, std::vector<std::string>{"Ego"});

    EXPECT_CALL(event, ExecuteStartTransition).Times(1);
    event.Step();
    event.Step();
    event.Step();
}

TEST_F(EventTestFixture, GivenEventWithUnsatisfiedStartTrigger_WhenStep_ThenEventAndActionAreNotStarted)
{
    auto by_value_condition = FakeByValueConditionBuilder{}
                                  .WithSimulationTimeCondition(FakeSimulationTimeCondition{
                                      NET_ASAM_OPENSCENARIO::v1_1::Rule::RuleEnum::EQUAL_TO, 10.0})
                                  .Build();
    auto fake_event = GetFakeEvent(by_value_condition);

    OPENSCENARIO::EventSUT event(fake_event, env_, std::vector<std::string>{"Ego"});

    auto actions = event.GetActions();
    event.Step();
    EXPECT_TRUE(event.isStandby());
    EXPECT_TRUE(actions[0]->isStandby());
}

}  // namespace OPENSCENARIO

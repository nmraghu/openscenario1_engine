/*******************************************************************************
 * Copyright (c) 2021 in-tech GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "UserDefinedValueCondition.h"

#include <iostream>

namespace OPENSCENARIO
{

bool UserDefinedValueCondition::IsSatisfied() const
{
    if (auto user_defined_value = environment_->GetUserDefinedValue(name_))
    {
        return rule_.IsSatisfied(std::move(user_defined_value.value()));
    }
    return false;
}

}  // namespace OPENSCENARIO

/*******************************************************************************
 * Copyright (c) 2021, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *                     in-tech GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include "EdgeEvaluatorBase.h"

#include <openScenarioLib/generated/v1_1/api/ApiClassInterfacesV1_1.h>

#include <memory>

namespace OPENSCENARIO
{

class NoEdgeEvaluator : public EdgeEvaluatorBase
{
  public:
    void UpdateStatus(bool is_satisfied) noexcept override;
};

class RisingEdgeEvaluator : public EdgeEvaluatorBase
{
  public:
    void UpdateStatus(bool is_satisfied) noexcept override;

  private:
    bool last_is_satisfied_{false};
};

class FallingEdgeEvaluator : public EdgeEvaluatorBase
{
  public:
    void UpdateStatus(bool is_satisfied) noexcept override;

  private:
    bool last_is_satisfied_{false};
};

class RisingOrFallingEdgeEvaluator : public EdgeEvaluatorBase
{
  public:
    void UpdateStatus(bool is_satisfied) noexcept override;

  private:
    bool last_is_satisfied_{false};
};

std::unique_ptr<EdgeEvaluatorBase> toEdgeEvaluator(const NET_ASAM_OPENSCENARIO::v1_1::ConditionEdge& conditionEdge);

}  // namespace OPENSCENARIO

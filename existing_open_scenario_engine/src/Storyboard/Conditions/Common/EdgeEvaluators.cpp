/*******************************************************************************
 * Copyright (c) 2021, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *                     in-tech GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "EdgeEvaluators.h"

namespace OPENSCENARIO
{

void NoEdgeEvaluator::UpdateStatus(bool is_satisfied) noexcept
{
    status_ = is_satisfied;
}

void RisingEdgeEvaluator::UpdateStatus(bool is_satisfied) noexcept
{
    status_ = (!last_is_satisfied_ && is_satisfied);
    last_is_satisfied_ = is_satisfied;
}

void FallingEdgeEvaluator::UpdateStatus(bool is_satisfied) noexcept
{
    status_ = (last_is_satisfied_ && !is_satisfied);
    last_is_satisfied_ = is_satisfied;
}

void RisingOrFallingEdgeEvaluator::UpdateStatus(bool is_satisfied) noexcept
{
    status_ = (last_is_satisfied_ != is_satisfied);
    last_is_satisfied_ = is_satisfied;
}

std::unique_ptr<EdgeEvaluatorBase> toEdgeEvaluator(const NET_ASAM_OPENSCENARIO::v1_1::ConditionEdge& conditionEdge)
{
    if (conditionEdge == NET_ASAM_OPENSCENARIO::v1_1::ConditionEdge::RISING)
    {
        return std::make_unique<RisingEdgeEvaluator>();
    }
    if (conditionEdge == NET_ASAM_OPENSCENARIO::v1_1::ConditionEdge::FALLING)
    {
        return std::make_unique<FallingEdgeEvaluator>();
    }
    if (conditionEdge == NET_ASAM_OPENSCENARIO::v1_1::ConditionEdge::RISING_OR_FALLING)
    {
        return std::make_unique<RisingOrFallingEdgeEvaluator>();
    }
    if (conditionEdge == NET_ASAM_OPENSCENARIO::v1_1::ConditionEdge::NONE)
    {
        return std::make_unique<NoEdgeEvaluator>();
    }
    throw std::runtime_error("Invalid condition edge.");
}

}  // namespace OPENSCENARIO

/*******************************************************************************
 * Copyright (c) 2021, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "CustomCommandAction.h"

namespace OPENSCENARIO
{

CustomCommandAction::CustomCommandAction(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::ICustomCommandAction> custom_command_action_data,
      std::shared_ptr<mantle_api::IEnvironment> environment,
      const std::vector<std::string>& actors)
    : UserDefinedAction(environment, actors),
    type_(custom_command_action_data->GetType()),
    command_(custom_command_action_data->GetContent())
{
}

void CustomCommandAction::Step()
{
    End();
}

void CustomCommandAction::ExecuteStartTransition()
{
    environment_->ExecuteCustomCommand(actors_, type_, command_);
}

}  // namespace OPENSCENARIO

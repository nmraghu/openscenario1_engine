/*******************************************************************************
 * Copyright (c) 2021, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include "Storyboard/Actions/Action.h"
#include "Storyboard/Conditions/Trigger.h"
#include "StoryboardElement.h"

#include <MantleAPI/Execution/i_environment.h>
#include <openScenarioLib/generated/v1_1/api/ApiClassInterfacesV1_1.h>

namespace OPENSCENARIO
{

class Event : public StoryboardElement
{
  public:
    explicit Event(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IEvent> event_data,
                   std::shared_ptr<mantle_api::IEnvironment> environment,
                   std::vector<std::string> actors);

    void Step() override;

  protected:
    std::vector<std::shared_ptr<Action>> actions;

  private:
    void ExecuteStartTransition() override;
    Trigger start_trigger_;
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_1::IEvent> data_;
};

}  // namespace OPENSCENARIO

/*******************************************************************************
 * Copyright (c) 2021, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "StoryboardRunningState.h"

namespace OPENSCENARIO
{

void StoryboardRunningState::Step()
{
    // step init actions (execute actions by calling the environment api and update internal state)

    // step stories (execute actions by calling the environment api and update internal state)

    // Check if stop trigger is met, and transition when true
    // context_->SetActiveState(*next_state_);
}

}  // namespace OPENSCENARIO
